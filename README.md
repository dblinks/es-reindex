# es-reindex #

es-reindex is a tool made for use with elasticsearch clusters. It helps you to reindex indexes across your cluster.

For example, it can be used when you need to change the version of a index. In this case is hard to send all your data again, es-reindex do that for you in a optimized way.

### How to run? ###

```
#!bash
$ java -jar es-reindex.jar <cluster-name> <source_index> <destination_index>
```

### How to build? ###

We use maven, so you just need to download the sources, build your project and execute the jar file in the target dir.