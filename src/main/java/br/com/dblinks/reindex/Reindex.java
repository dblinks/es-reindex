package br.com.dblinks.reindex;

import java.math.BigDecimal;
import java.util.Optional;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsRequest;
import org.elasticsearch.action.admin.indices.stats.IndicesStatsResponse;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.lang3.StringUtils;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import static org.elasticsearch.index.query.QueryBuilders.*;
import org.elasticsearch.search.SearchHit;


public class Reindex {

    private final String cluster;
    private final String src;
    private final String dest;
    
    private Long documentsToIndex;
    private Long documentsIndexed = new Long("0");
    
    private Client client;

    public Reindex(String cluster, String src, String dest) {
        this.cluster = cluster;
        this.src = src;
        this.dest = dest;
    }
    
    public void start(){
        this.client = createClient();
        this.documentsToIndex = getCountOfDocumentsInSource();
        
        BulkRequestBuilder bulkRequest = client.prepareBulk();

        SearchResponse response = client.prepareSearch(src)
                .setSearchType(SearchType.SCAN)
                .setScroll(new TimeValue(60000))
                .setQuery(matchAllQuery())
                .setSize(5000).execute().actionGet();
        while (true) {
            response = client.prepareSearchScroll(response.getScrollId())
                    .setScroll(new TimeValue(60000)).execute().actionGet();
            for (SearchHit hit : response.getHits()) {
                bulkRequest.add(
                        client.prepareIndex(dest, hit.getType(), hit.getId())
                        .setSource(hit.getSourceAsString())
                );
                documentsIndexed++;
                
                writeProgressBar();
            }

            if (response.getHits().getHits().length == 0) {
                BulkResponse bulkResponse = bulkRequest.execute().actionGet();
                if (bulkResponse.hasFailures()) {
                    System.out.println("Ocorreu um erro ao executar o bulk insert");
                }
                break;
            }
        }
    
    }
    
    private Long getCountOfDocumentsInSource() {
        IndicesStatsResponse stats = client.admin().indices().prepareStats()
                .setIndices(this.src)
                .execute().actionGet();
        
        return stats.getIndex(this.src).getTotal().getDocs().getCount();
    }

    private Client createClient() {
        Settings settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", cluster).build();

         return new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
    }
    
    private void writeProgressBar() {
        BigDecimal percentage = new BigDecimal("" + ((documentsIndexed * 100) / documentsToIndex));

        String progressBar = "";
        for (int i = 0; i < percentage.intValue(); i++) {
            progressBar += "=";
        }
        progressBar = StringUtils.rightPad(progressBar, 100, ' ');
        System.out.print("[" + progressBar + "] " + percentage.intValue() + "% => " + documentsIndexed + " docs indexed \r");
    }
    
    public static void main(String[] args){
        new Reindex(args[0], args[1], args[2]).start();
    }
}
